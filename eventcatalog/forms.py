from django import forms
from django.utils import timezone
from userauth.models import Event

class EventForm(forms.ModelForm):
    nama_event = forms.CharField(
        widget=forms.TextInput(), 
        max_length=100
    )
    details = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder':'informasi detail event'}
            ),
        max_length=1000
    )
    jadwal = forms.DateTimeField(
        widget=forms.TextInput(
            attrs={'type': 'datetime-local'}
        ),
        input_formats=["%m/%d/%Y %H:%M"]
    )
        
    def clean_jadwal(self):
        data = self.cleaned_data["jadwal"]
        if timezone.now() >= data:
            raise forms.ValidationError(u'Tanggal/waktu anda tidak valid!! "%s"' % data)
        return data

    
    class Meta:
        model = Event
        fields = ['nama_event', 'jadwal', 'details']
