from userauth.models import Event
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User

# Create your tests here.
class CatalogTest(TestCase):
    """
    Tests for event catalogs
    """
    
    def test_catalog_response_status(self):
        response = self.client.get('/events', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_create_event_access(self):
        response = self.client.get('/events/new')
        self.assertEqual(response.status_code, 302)

        user = User.objects.create_user(
            username="dummy test",
            email="abc@xyz.com",
            password="solo-speedrun-uyey",
        )

        self.client.login(
            username="dummy test",
            password="solo-speedrun-uyey",
        )

        response = self.client.get('/events/new')
        self.assertEqual(response.status_code, 200)

    def test_event_creation(self):
        user = User.objects.create_user(
            username="dummy test 2",
            email="abc@xyz.com",
            password="solo-speedrun-uyey",
        )

        self.client.login(
            username="dummy test 2",
            password="solo-speedrun-uyey",
        )
        
        event_data = {
            "nama_event": "dummy event",
            "jadwal": timezone.datetime(2024, 12, 8, 10, 30),
            "details": "this is a dummy event",
        }

        response = self.client.post('/events/new', data=event_data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/events/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event_data["nama_event"])
        
    def test_event_out_of_date(self):
        user = User.objects.create_user(
            username="dummy test 5",
            email="abc@xyz.com",
            password="solo-speedrun-uyey",
        )

        self.client.login(
            username="dummy test 5",
            password="solo-speedrun-uyey",
        )

        event_data = {
            "nama_event": "dummy event",
            "jadwal": timezone.datetime(1999, 12, 8, 10, 30),
            "details": "this is a dummy event",
        }

        response = self.client.post('/events/new', data=event_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "tidak valid")
        
    def test_event_join(self):
        user = User.objects.create_user(
            username="dummy test 3",
            email="abc@xyz.com",
            password="solo-speedrun-uyey",
        )
        
        user2 = User.objects.create_user(
            username="dummy test 4",
            email="abc@xyz.com",
            password="solo-speedrun-uyey",
        )
        
        self.client.login(
            username="dummy test 3",
            password="solo-speedrun-uyey",
        )
        
        event = Event.objects.create(
                nama_event = "dummy event",
                jadwal = timezone.datetime(2024, 12, 8, 10, 30),
                details = "this is a dummy event",
        ) 
        
        response = self.client.get(
            reverse('eventcatalog:view', args=(event.id,))
        )
        
        self.assertContains(response, event.nama_event)
        self.assertContains(response, "joined this event")
        self.assertContains(response, "Back to Events")
        
        self.client.logout()
        self.client.login(
            username="dummy test 4",
            password="solo-speedrun-uyey",
        )
        
        response = self.client.get(
            reverse('eventcatalog:view', args=(event.id,))
        )

        self.assertContains(response, event.nama_event)
        self.assertContains(response, "Join Event")
        self.assertContains(response, "Back to Events")
        
        response = self.client.get(
            reverse('eventcatalog:join', args=(event.id,))
        )
        
        self.assertContains(response, "Successfully")
