from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from userauth.models import Event, Organizer
from .forms import EventForm

# Create your views here.

def index(request):
    context = {
        'events': Event.objects.all()
    }
    return render(request, 'eventcatalog-index.html', context=context)

@login_required
def new(request):
    context = {}
    if request.method == 'POST':
        form = context['form'] = EventForm(request.POST)
        if form.is_valid():
            event = form.save(commit=False)
            event.penyelenggara, created = Organizer.objects.get_or_create(
                user=request.user
            )
            event.save()
            return redirect('eventcatalog:index')
    
    else:
        context['form'] = EventForm()
        
    return render(request, 'eventcatalog-add.html', context=context)

def view(request, id):
    context = {
        'disable': False
    }
    
    event = Event.objects.get(id=id)
    context['event'] = event
    if request.user in event.peserta.all() or request.user == event.penyelenggara.user:
        context['disable'] = True
    return render(request, 'eventcatalog-view.html', context=context)

@login_required
def join(request, id):
    context = {
        "join": True,
        "disable": True
    }
    event = Event.objects.get(id=id)
    context['event'] = event
    event.peserta.add(request.user)
    context['join_success'] = True
    return render(request, 'eventcatalog-view.html', context=context)
