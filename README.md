# Temu - Your Events Around You
## Tugas Kelompok 1 -- B13

[![pipeline status](https://gitlab.com//Artemis1872/TK1-B13/badges/master/pipeline.svg)](https://gitlab.com/Artemis1872/TK1-B13/-/commits/master)
[![coverage report](https://gitlab.com//Artemis1872/TK1-B13/badges/master/coverage.svg)](https://gitlab.com/Artemis1872/TK1-B13/-/commits/master)



## Anggota kelompok
1. Dennis Al Baihaqi Walangadi (`1906400141`)
2. Galuh Vita Respati (`1906399404`)
3. Muhammad Barry Haikal (`1906399801`)
4. Muhammad Nafis Hibatullah (`1906399026`)

## [`Link Heroku`](https://tk1-b13.herokuapp.com/)

## Cerita Aplikasi
Pandemi memisahkan jarak antara orang-orang dengan aktivitas yang mereka senangi. Seperti kegiatan perkumpulan, belajar bersama, hobi, dan lain sebagainya.  

Temu merupakan website yang dapat memudahkan orang-orang yang memiliki minat yang sama untuk bertemu tanpa harus keluar rumah dan terpapar pandemi.   

Melalui Temu, orang-orang dapat saling terhubung untuk menyalurkan minat mereka tanpa harus bertemu secara langsung untuk menikmati setiap momen selama pandemi. 

## Fitur
- Make events
- Book events
- User Auth (registration, login, logout)
- Tag events
- Search event
- User dashboard
- Generate google meet link

## TODO:
- [x] Navbar header
- [ ] Footer
- [ ] Landing page
- [ ] Event list
- [ ] Event detail
- [ ] New event form
- [x] Login page
- [x] User Model