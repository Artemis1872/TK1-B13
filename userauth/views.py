from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import  AuthenticationForm
from .forms import CreateUserForm

def register(request):
    context = {
        'auth': True
    }
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        context['form'] = form
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Registrasi berhasil, silahkan login kembali')
            return redirect('userauth:login')

    else:
        context['form'] = CreateUserForm()
        
    return render(request, 'userauth-register.html', context=context)

def user_logout(request):
    logout(request)
    return redirect('userauth:login')
