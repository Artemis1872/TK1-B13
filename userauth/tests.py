from .models import Event, Organizer
from django.utils import timezone
from django.test import TestCase
from django.contrib.auth import login, authenticate, get_user
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

# Create your tests here.
class AuthTest(TestCase):
    """
    Tests for authentication logics.
    """
    # TODO: Test for django messages, test login errors

    def test_register_password_didnt_match(self):
        register_data = {
            "username": "test_dummy_different_pass",
            "password1": "abc5dasar",
            "password2": "abc5dodol"
        }
        
        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/account/register', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "two password fields didn’t match")

    def test_register_username_taken(self):
        register_data = {
            "username": "test_dummy_duplicate",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/account/register', data=register_data)
        self.assertEqual(response.status_code, 302)
        
        response = self.client.post('/account/register', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "user with that username already exists")

    def test_register_password_too_common(self):
        register_data = {
            "username": "test_dummy_too_common",
            "password1": "asdf12345",
            "password2": "asdf12345"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/account/register', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "password is too common")
        
    def test_success_register_login_logout(self):
        register_data = {
            "username": "test_dummy",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/account/register', data=register_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/account/login')

        # Login user
        login_data = {
            "username": "test_dummy",
            "password": "abc5dasar"
        }
        response = self.client.post('/account/login', data=login_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/')
        
        # Check auth status after login
        user = get_user(self.client)
        self.assertTrue(user.is_authenticated)

        # Logout user
        response = self.client.get('/account/logout')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/account/login')
        
        # Check auth status after logout
        user = get_user(self.client)
        self.assertFalse(user.is_authenticated)

class ModelAssociationTest(TestCase):
    """
    Tests for event and user models.
    """
    
    def test_event_model(self):
        event = Event.objects.create(
            nama_event="dummy event",
            jadwal=timezone.now(),
            details="this is a test"
        )
        
        self.assertEqual(str(event), event.nama_event)
        self.assertEqual(event.nama_event, "dummy event")
        self.assertEqual(event.details, "this is a test")
    
    def test_organizer_model(self):
        user = User.objects.create_user(
            username="dummy test",
            email="abc@xyz.com",
            password="abcxyzaleale",
        )
        
        event = Event.objects.create(
            nama_event="dummy event",
            jadwal=timezone.now(),
            details="this is a test"
        )
        
        organizer, created = Organizer.objects.get_or_create(
            user=user
        )
        
        self.assertEqual(str(organizer), user.username)
        self.assertEqual(organizer, event.penyelenggara)
    
    def test_event_and_organizer_relationship(self):
        pass
