from django.urls import path
from django.contrib.auth import views as auth_views
from . import views

app_name = 'userauth'

urlpatterns = [
    path('login', auth_views.LoginView.as_view(
        template_name='userauth-login.html', 
        redirect_field_name='mainpage:index', 
        extra_context={'auth': True}
        ), name = 'login'),
    path('register', views.register, name='register'),
    path('logout', views.user_logout, name='logout'),
]
