from django.db import models
from django.contrib.auth.models import User


def get_default_organizer():
    return Organizer.objects.get_or_create(
        user=User.objects.get_or_create(id=1)[0]
        )[0]

# Create your models here.
class Organizer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.user.username

class Event(models.Model):
    nama_event = models.CharField(max_length=30)
    jadwal = models.DateTimeField()
    details = models.TextField(max_length=1000, default="")
    peserta = models.ManyToManyField(User, blank=True)
    penyelenggara = models.ForeignKey(
        Organizer, on_delete=models.CASCADE, default=get_default_organizer)
    
    waktu_dibuat = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nama_event
