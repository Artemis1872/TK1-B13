from django.test import TestCase
from django.urls import reverse

# Create your tests here.

class MainTestCase(TestCase):
    """
    Tests for page availability. 
    """
    
    def test_root_http_status_200(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)
    
    def test_userregister_http_status_200(self):
        response = self.client.get("/account/register")
        self.assertEqual(response.status_code, 200)
